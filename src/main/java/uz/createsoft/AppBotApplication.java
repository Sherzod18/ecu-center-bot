package uz.createsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AppBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppBotApplication.class, args);
    }

}
