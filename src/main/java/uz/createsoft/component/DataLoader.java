package uz.createsoft.component;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.createsoft.entity.Course;
import uz.createsoft.repository.CourseRepository;


import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;


    @Autowired
    CourseRepository courseRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            Course course = new Course();
            course.setCourseName("Spring boot");
            course.setCourseTex("java core, DB");
            course.setCourseDate("6 Oy");
            course.setCourseTime("2 soat");
            course.setPrice("400.000 so'm");
            courseRepository.save(course);
            Course course2 = new Course();

            course2.setCourseName("Backend");
            course2.setCourseTex("php, yii2, DB");
            course2.setCourseDate("6 Oy");
            course2.setCourseTime("2 soat");
            course2.setPrice("400.000 so'm");
            courseRepository.save(course2);
            Course course3 = new Course();

            course3.setCourseName("Frontend");
            course3.setCourseTex("html, css, bootstrap");
            course3.setCourseDate("3 Oy");
            course3.setCourseTime("2 soat");
            course3.setPrice("400.000 so'm");
            courseRepository.save(course3);

        }
    }


}
