package uz.createsoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.createsoft.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Integer> {
}
