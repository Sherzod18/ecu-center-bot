package uz.createsoft.bot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@Component
public class CSAcadeBot extends TelegramLongPollingBot {

    @Autowired
    TelegramService telegramService;

    @Autowired
    BotUserRepository botUserRepository;

    @Value("${bot.token}")
    private String botToken;
    @Value("${bot.username}")
    private String botUsername;


    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            if (update.getMessage().hasText()){
                Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
                String text = update.getMessage().getText();

                if (text.equals("/start")) {
                    System.out.println(text);
                    try {
                        execute(telegramService.showMenu(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }

        }else if (update.hasCallbackQuery()){
            if (update.getCallbackQuery().getData().equals("Profile")){
                System.out.println("Profile");
            }else if (update.getCallbackQuery().getData().equals("Course")){
                System.out.println("Course");

                try {
                    execute(telegramService.showAllCourses(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }

            }else if (update.getCallbackQuery().getData().equals("Address")){
                System.out.println("Address");
                try {
                    execute(telegramService.deleteTopMessage(update));
                    execute(telegramService.showAddress(update));
                    execute(telegramService.backMain2(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }else if (update.getCallbackQuery().getData().equals("BackMain2")){
                try {
                    execute(telegramService.deleteTopMessage(update));
                    execute(telegramService.backMain2(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }

//        System.out.println(update);
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
