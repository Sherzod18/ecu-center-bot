package uz.createsoft.bot;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import uz.createsoft.entity.Course;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BotUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(unique = true)
    private String phoneNumber;

    private String firstName;

    private String lastName;

    private String state;

    private Integer messageId;

    private String rekId;

    private Long chatId;

    private String seatId;

    private String expireDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Course course;

    private Date birthDate;

    public Integer tgUserId;

}
