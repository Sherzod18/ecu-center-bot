package uz.createsoft.bot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.createsoft.entity.Course;
import uz.createsoft.repository.CourseRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TelegramServiceImpl implements TelegramService {

    @Autowired
    BotUserRepository botUserRepository;

    @Autowired
    CourseRepository courseRepository;


    @Override
    public SendMessage showMenu(Update update) {

        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());

        if (!botUser.isPresent()) {
            BotUser botUser1 = new BotUser();
            botUser1.setTgUserId(update.getMessage().getFrom().getId());
            botUserRepository.save(botUser1);
        }

        String ADDRESS_US_KR = "Бизнинг манзил";

        String CONNECT_TO_ADMIN_KR = "Админ билан боғланиш";
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN).setChatId(update.getMessage().getChatId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();
        button2.setText("Курслар");
        button3.setText(CONNECT_TO_ADMIN_KR);
        button4.setText(ADDRESS_US_KR);

        button2.setCallbackData("Course");
        button3.setUrl("https://t.me/khalilovabbos");
        button4.setCallbackData("Address");

        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        sendMessage.setText("Botga hush kelibsiz ");
        return sendMessage;

    }

    @Override
    public SendLocation showAddress(Update update) {
        SendLocation sendLocation = new SendLocation();
        sendLocation.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setLongitude(65.784897f).setLatitude(38.845825f);
        return sendLocation;
    }

    @Override
    public SendPhoto showImage(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();

        button.setText("⬅️Орқага");

        button.setCallbackData("BackMain2");

        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        try {
            sendPhoto.setPhoto("impulse.jpg", new ClassPathResource("/impulse.jpg").getInputStream());
//            sendPhoto.setCaption("\uD83C\uDFE2Манзил: *Тошкент шаҳар, Миробод тумани, Авлиё Ота кўчаси-4, Техномарт биноси пастки қаватида*.\n\n\uD83D\uDCCDМўлжал: *Миробод бозори*.\n");
            sendPhoto.setCaption("Bizni Uquv markaz.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        row.add(button);
        rows.add(row);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        return sendPhoto;
    }

    @Override
    public EditMessageText setPhoneNumber(Update update) {
        return null;
    }

    @Override
    public EditMessageText showAllCourses(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();

        StringBuilder stringBuilder = new StringBuilder();

        List<Course> courseList = courseRepository.findAll();
        stringBuilder.append("\uD83D\uDD25 Kurslar ruyxati\n\n");
        courseList.forEach(course -> {
            stringBuilder.append("\uD83D\uDD25 ")
                    .append("Kurs nomi: " + course.getCourseName() + "\n")
                    .append("Tex: " + course.getCourseTex())
                    .append(" \uD83D\uDD39 ")
                    .append(" \n\uD83D\uDD54").append("muddati: " + course.getCourseDate() + " vaqti: " + course.getCourseTime() + "\n")
                    .append("narxi: " + course.getPrice()).append("\n\n");
        });


        editMessageText.setText(stringBuilder.toString());

        button.setText("⬅️Орқага");

        button.setCallbackData("BackMain2");

        row.add(button);

        rows.add(row);
        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public DeleteMessage deletePhoneNumber(Update update) {
        return null;
    }

    @Override
    public EditMessageText setFirstName(Update update) {
        return null;
    }

    @Override
    public DeleteMessage deleteFirstNameMessage(Update update) {
        return null;
    }

    @Override
    public EditMessageText setLastName(Update update) {
        return null;
    }

    @Override
    public DeleteMessage deleteLastNameMessage(Update update) {
        return null;
    }

    @Override
    public EditMessageText setBirthDate(Update update) {
        return null;
    }

    @Override
    public DeleteMessage deleteBirthDate(Update update) {
        return null;
    }

    @Override
    public EditMessageText backMain(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        String ADDRESS_US_KR = "Бизнинг манзил";

        String CONNECT_TO_ADMIN_KR = "Админ билан боғланиш";
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();

        EditMessageText editMessageText = new EditMessageText().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());

        editMessageText.setText("Сизнинг ID: " + update.getCallbackQuery().getFrom().getId() + "\n\n" +
                "@csacademy_bot га хуш келибсиз!\n\n" +
                "➡️" + " @csacademy_uz" + " каналида янгиликларимизни кузатиб боринг\n\n");
        button2.setText("Курслар");
        button3.setText(CONNECT_TO_ADMIN_KR);
        button4.setText(ADDRESS_US_KR);

        button2.setCallbackData("Course");
        button3.setUrl("https://t.me/khalilovabbos");
        button4.setCallbackData("Address");
        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);

        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendMessage backMain2(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();


        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();

        SendMessage sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        String ADDRESS_US_KR = "Бизнинг манзил";

        String CONNECT_TO_ADMIN_KR = "Админ билан боғланиш";
        sendMessage.setText("Сизнинг ID: " + update.getCallbackQuery().getFrom().getId() + "\n\n" +
                "@csacademy_bot га хуш келибсиз!\n\n" +
                "➡️" + " @csacademy_uz" + "  каналида янгиликларимизни кузатиб боринг\n\n");

        button2.setText("Курслар");
        button3.setText(CONNECT_TO_ADMIN_KR);
        button4.setText(ADDRESS_US_KR);

        button2.setCallbackData("Course");
        button3.setUrl("https://t.me/khalilovabbos");
        button4.setCallbackData("Address");
        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);

        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public EditMessageText myProfile(Update update) {
        return null;
    }

    @Override
    public SendMessage showMyProfile(Update update) {
        return null;
    }

    @Override
    public DeleteMessage deleteTopMessage(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        DeleteMessage deleteMessage = new DeleteMessage();
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return deleteMessage;
    }
}
